#include "fir.h"


int fir (unsigned int Muestra)
{
	unsigned int i;
	float y = 0;
	static int x [ORDEN_FILTRO] = {0};
	static int pointer = 0;
	unsigned int auxiliar;
	float b [ORDEN_FILTRO] = { ((float) 1) / ORDEN_FILTRO, ((float) 1) / ORDEN_FILTRO , ((float) 1) / ORDEN_FILTRO , ((float) 1) / ORDEN_FILTRO};

	x [pointer] = Muestra;

	for (i = pointer; i < (pointer + ORDEN_FILTRO) ; i ++ )
	{
		y += x[i % ORDEN_FILTRO] * b[i-pointer];
	}

	pointer = (pointer + 1) % ORDEN_FILTRO;

	return ((int) y);
}
